# Rpg game in JAVA

A simple rpg template with four different classes and functions to create items and armors to equip. It is very easy to change or add your own unique class/items.

## Installation
How to download this and get started:
````
download zip
git clone git@gitlab.com:Robinax/rpg-game-in-java.git
````
When you downloaded it you can run it on Intellij (SDK17)

## Usage
Could be used as an base template for a easy RPG game.

## Contributing
@Robinax
