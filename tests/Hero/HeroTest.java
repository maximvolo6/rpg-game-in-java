package Hero;
import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Hero.Classes.Mage;
import Hero.Classes.Ranger;
import Hero.Classes.Rogue;
import Hero.Classes.Warrior;
import Items.Armors.Armor;
import Items.Items;
import Items.Weapons.Weapon;
import Utils.ArmorTypes;
import Utils.Slots;
import Utils.WeaponTypes;
import org.junit.jupiter.api.Test;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class HeroTest {
    @Test
    public void checkIfHeroGetsRightAttributes() {
        //Arange
        Rogue rogue = new Rogue("bob");
        int[] rogueAttribute = rogue.getAttribute();
        int[] expectedAttributes ={2, 6, 1};
        int[] expectedAttributesAfterLevelUp ={3, 10, 2};
        //Act
        rogue.levelUp();
        int[] rogueAttributeAfterLevelUp = rogue.getAttribute();
        //Assert
        assertEquals(Arrays.toString(expectedAttributes),Arrays.toString(rogueAttribute));
        assertEquals(Arrays.toString(rogueAttributeAfterLevelUp),Arrays.toString(expectedAttributesAfterLevelUp));
    }
    @Test
    public void CheckIfHeroCanLevelUp(){
    //Arrange
        Mage mage = new Mage("bob");
        int levelOfMage = mage.level;
        int expectedLevelOfMage = 1;
        int expectedLevelOfMageAfterLevelUp = 2;

        //Act
        mage.levelUp();
        int levelOfMageAfterLevelUp = mage.level;
        //Assert
        assertEquals(expectedLevelOfMage,levelOfMage);
        assertEquals(expectedLevelOfMageAfterLevelUp,levelOfMageAfterLevelUp);
    }

    @Test
    public void checkIfHeroCanEquipItems(){
        //Arrange
        Rogue rogue = new Rogue("bob");
        var noItemEquiped = rogue.getEquippedItems(Slots.WEAPON);
        Weapon dagger = new Weapon("Ice Blade",0,Slots.WEAPON,
                WeaponTypes.DAGGER,15,1.3,new int[]{0,2,0});
        Items noItemEquipedExpected = null;
        String weaponNameExpected = dagger.getName();

        boolean equipExpecter = true;
        //Act
        try {
            rogue.equipWeapon(dagger);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        Items weaponEquiped = rogue.getEquippedItems(Slots.WEAPON);

        //Assert
        assertEquals(noItemEquipedExpected,noItemEquiped);
        assertEquals(weaponNameExpected,weaponEquiped.getName());
        try {
            assertEquals(equipExpecter,rogue.equipWeapon(dagger));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void checkIfItemsGiveStats(){
        //Arrange
        Rogue rogue = new Rogue("bob");
        int[] rogueAttributeBeforeItemEquiped = rogue.getAttribute();
        int[] expectedAttributesBeforeItemEquiped ={2, 6, 1};

        Weapon dagger = new Weapon("Ice Blade",0,Slots.WEAPON,
                WeaponTypes.DAGGER,15,1.3,new int[]{0,2,0});

        int[] expectedAttributesAfterItemEquied = {2,8,1};
        //Act
        try {
            rogue.equipWeapon(dagger);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        int[] rogueAttributeAfterItemEquiped = rogue.getAttribute();
        //Assert
        assertEquals(Arrays.toString(expectedAttributesBeforeItemEquiped),Arrays.toString(rogueAttributeBeforeItemEquiped));
        assertEquals(Arrays.toString(expectedAttributesAfterItemEquied),Arrays.toString(rogueAttributeAfterItemEquiped));

    }
    @Test
    public void checkWrongArmorTypeExceptions(){
        //Arrange
        Warrior warrior = new Warrior("Conan");
        Armor clothArmor = new Armor("Robe of Doom",0,Slots.BODY,
                ArmorTypes.CLOTH,new int[]{0,0,25});
        Armor platedMail = new Armor("Plated mail",2,Slots.BODY,
                ArmorTypes.PLATE,new int[]{25,0,0});

        //Act && Assert
        assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(clothArmor));
        assertThrows(InvalidArmorException.class, () -> warrior.equipArmor(platedMail));

    }
    @Test
    public void checkWrongWeaponTypeExceptions(){
        //Arrange
        Warrior warrior = new Warrior("Conan");
        Weapon excaliber = new Weapon("Excalibur",43,Slots.WEAPON,
                WeaponTypes.SWORD,84,2,new int[]{43,32,15});
        Weapon huntingBow = new Weapon("Hunting bow",1,Slots.WEAPON,
                WeaponTypes.BOW,10,1.5,new int[]{0,4,0});

        //Act && Assert
        assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(excaliber));
        assertThrows(InvalidWeaponException.class, () -> warrior.equipWeapon(huntingBow));

    }
    @Test
    public void CheckIfValidArmorCanEquip(){
        //Arrange
        Ranger ranger = new Ranger("Tael");
        Armor leatherLegs = new Armor("Leather Legs",1,Slots.LEGS,
                ArmorTypes.LEATHER,new int[]{0,3,1});
        boolean equipExpectedToWork = true;

        //Act && Assert
        try {
            assertEquals(equipExpectedToWork,ranger.equipArmor(leatherLegs));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }
    @Test
    public void CheckDpsWithoutWeapon(){
        //Arrange
        Warrior warrior = new Warrior("Blade");
        double characterDps = warrior.getCharacterDps();
        double expectedDps = 2 *(1+(5/(double)100));
        //Act

        //Assert
        assertEquals(expectedDps,characterDps);
    }
    @Test
    public void CheckDpsWithValidWeaponEquipped(){
        //Arrange
        Mage mage = new Mage("Gandalf");
        Weapon whiteStaff = new Weapon("White Staff",1,Slots.WEAPON,
                WeaponTypes.STAFF,23,3,new int[]{1,0,14});
        double expectedDPS = (23 * 3)*(1+(22/(double)100));

        //Act
        try {
            mage.equipWeapon(whiteStaff);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        //Assert
        assertEquals(expectedDPS,mage.getCharacterDps());
    }
    @Test
    public void CheckDpsWithBothWeaponAndArmorEquipped(){
        //Arrange
        Rogue rogue = new Rogue("Shadow");
        Weapon dagger = new Weapon("Shadow Dagger",1,Slots.WEAPON,
                WeaponTypes.DAGGER,10,2.2,new int[]{0,2,0});
        Armor leatherArmor = new Armor("Leather Armor",1,Slots.BODY,
                ArmorTypes.LEATHER,new int[]{0,2,0});
        double expectedDPS = (10*2.2)*(1+(10/(double)100));
        //Act
        try {
            rogue.equipWeapon(dagger);
            rogue.equipArmor(leatherArmor);

        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        //Assert
        assertEquals(expectedDPS,rogue.getCharacterDps());

    }
}