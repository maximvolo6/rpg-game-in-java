package Utils;

public enum WeaponTypes {
    //Enum for weapon types. Easy to add more if that is wanted.
    AXE,HAMMER,BOW,STAFF,WAND,DAGGER,SWORD,NONE
}
