package Hero.Classes;

import Hero.Hero;
import Hero.PrimaryAttribute;
import Utils.ArmorTypes;
import Utils.PrimaryStat;
import Utils.WeaponTypes;

public class Mage extends Hero {
    public Mage(String name) {
        super(name, 1, 1, 1, 8 ,
                new ArmorTypes[]{ArmorTypes.CLOTH,ArmorTypes.NONE}, new WeaponTypes[]{WeaponTypes.STAFF,WeaponTypes.WAND,WeaponTypes.NONE}
        , PrimaryStat.INT);
    }


//Level up function for the classes. The all got different stats when they level up so its in their
    //Classes we can set it to what we want.
    public void levelUp(){

        this.setAttribute(new PrimaryAttribute(1,1,5));
        this.level ++;
    }





}
