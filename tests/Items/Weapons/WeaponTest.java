package Items.Weapons;

import Utils.Slots;
import Utils.WeaponTypes;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WeaponTest {

    @Test
    public void checkValueOfWeapon(){
        //Arrange
        Weapon hammer = new Weapon("Hammer of justice", 22, Slots.WEAPON,
                WeaponTypes.HAMMER,25,2.2,new int[]{2,2,2});

        String expectedName = "Hammer of justice";
        int expectedLevel = 22;
        Slots expectedSlot = Slots.WEAPON;
        WeaponTypes expectedType = WeaponTypes.HAMMER;
        int expectedDmg = 25;
        double expectedAttackSpeed = 2.2;
        int[] expectedStatBoost = {2,2,2};

        //Act && Assert
        assertEquals(expectedName,hammer.getName());
        assertEquals(expectedLevel,hammer.getLevelRequirement());
        assertEquals(expectedSlot,hammer.getEquipSlot());
        assertEquals(expectedType,hammer.getType());
        assertEquals(expectedDmg,hammer.getDmg());
        assertEquals(expectedAttackSpeed,hammer.getAttackSpeed());
        assertEquals(expectedStatBoost[0],hammer.getStatIncrease(0));
        assertEquals(expectedStatBoost[1],hammer.getStatIncrease(1));
        assertEquals(expectedStatBoost[2],hammer.getStatIncrease(2));
    }

}