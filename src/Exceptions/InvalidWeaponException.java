package Exceptions;

public class InvalidWeaponException extends Exception{
    //Exception for invalid weapon expressions
   public InvalidWeaponException(String errorMessage){
        super(errorMessage);
    }
}
