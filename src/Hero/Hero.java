package Hero;

import Exceptions.InvalidArmorException;
import Exceptions.InvalidWeaponException;
import Items.Armors.Armor;
import Items.Items;
import Items.Weapons.Weapon;
import Utils.ArmorTypes;
import Utils.PrimaryStat;
import Utils.Slots;
import Utils.WeaponTypes;

import java.util.HashMap;

public abstract class Hero implements CharacterFunction{
    //Variables needed for all the classes to function and to be uniqe

    private String name;

    private WeaponTypes typeWeapons[];
    private ArmorTypes typeArmor[];
    private int strStat;
    private int dexStat;
    private int intStat;
    private double baseDamage = 2;
    protected int level;
    private PrimaryStat specificPrimaryStat;
    //HashMap for equiped items. There can only be one key(slot) in a hashmap so it can never take in
    //2x of the same keys without overwriting it. Perfect for this.
    private HashMap<Slots, Items> equipment = new HashMap<>();
    //Constructor of Hero class
    public Hero(String name, int level, int strStat, int dexStat, int intStat, ArmorTypes[] typeArmor, WeaponTypes[] typeWeapons
    , PrimaryStat specificPrimaryStat) {
        this.name = name;
        this.strStat = strStat;
        this.dexStat = dexStat;
        this.intStat = intStat;
        this.level = level;
        this.typeArmor = typeArmor;
        this.typeWeapons = typeWeapons;
        this.specificPrimaryStat = specificPrimaryStat;
    }
    //Setting the attributes with the help of PrimaryAttribute Class
    public void setAttribute(PrimaryAttribute stats) {
        this.strStat += stats.getPrimaryAttributeStr();
        this.dexStat += stats.getPrimaryAttributeDex();
        this.intStat += stats.getPrimaryAttributeInt();
    }
    //Get all the attributes in an array str,dex,int
    public int[] getAttribute(){
        return new int[]{this.strStat,this.dexStat,this.intStat};
    }
    //Function to check what primary stat is for each class with the help of an enum
    public int getPrimaryAttribute(){
       if (getSpecificPrimaryStat().equals(PrimaryStat.INT)){return this.intStat;}
       if (getSpecificPrimaryStat().equals(PrimaryStat.STR)){return this.strStat;}
       if (getSpecificPrimaryStat().equals(PrimaryStat.DEX)){return this.dexStat;}

     return 0;
    }
    //Set the equipped items. which item and which slot you want to set it on.
    public void setEquippedItems(Items equipment, Slots slot) {
        this.equipment.put(slot, equipment);

    }
    //Get equipped items depending on what slot you want to get.
    public Items getEquippedItems(Slots slot) {
        if(equipment.get(slot) != null) {
            return equipment.get(slot);
        }
        return null;
    }
    //Equips weapon and checks if the class can equip it and if the level is right.
    @Override
    public boolean equipWeapon(Weapon weapon) throws Exception {
        //Checks if the weapon lvl is greater than the heroes lvl
        if(weapon.getLevelRequirement() > this.level){
            throw new InvalidWeaponException("The level requirement for the weapon is higher than your lvl!");
        }
        for (int i = 0; i< typeWeapons.length;i++){
            if (weapon.getType().equals(typeWeapons[i])){
                setEquippedItems(weapon, weapon.getEquipSlot());
                this.setAttribute(new PrimaryAttribute(weapon.getStatIncrease(0), weapon.getStatIncrease(1),weapon.getStatIncrease(2) ));
                return true;
            }
        }
        throw new InvalidWeaponException("You cant equip this type of weapon!");
    }
    //Function to equip armor. Checks if level is right and if the class can use that armor
    public boolean equipArmor(Armor armor) throws Exception {
        //Checks if the armor level req is higher than the heroes level.
        if (armor.getLevelRequirement() > this.level){
            throw new InvalidArmorException("The level requirement for the armor is higher than your lvl!");
        }
        //For loop to check if the hero can use that type of armor
        for (int i = 0; i< typeArmor.length;i++){
            if(armor.getType().equals(typeArmor[i])) {
                setEquippedItems(armor, armor.getEquipSlot());
                this.setAttribute(new PrimaryAttribute(armor.getStatIncrease(0), armor.getStatIncrease(1),armor.getStatIncrease(2) ));
                return true;
            }
        }
        //If all the hero cant use that type we return false
        throw new InvalidArmorException("You cant equip this type of armor!");
    }
    //Function to unequip armor/weapon depending on slot. does - on stats that the item gave.
    @Override
    public boolean unequipItem(Slots slot){
        if (slot == null){
            return false;
        }
        var tempItem = getEquippedItems(slot);
        this.setAttribute(new PrimaryAttribute(-tempItem.getStatIncrease(0), -tempItem.getStatIncrease(1),-tempItem.getStatIncrease(2) ));
        setEquippedItems(null,slot);
        return true;
    }
    //Function to get the details of the created hero.
    @Override
    public void getDetails() {
        System.out.println("Name:" +this.name +'\n'+"Level:" +this.level + '\n' +"str:" + this.strStat + '\n' + "dex:" + this.dexStat + '\n' + "int:" + this.intStat + '\n' + "DPS:" + getCharacterDps());
    }
    //Gets the primaryStat. Mage Int/Warrior str and so on.
    public PrimaryStat getSpecificPrimaryStat() {
        return specificPrimaryStat;
    }
    //Gets the character dps. If a weapon is not equipped it gets the basedamage dps.
    public double getCharacterDps(){
       var tempWeapon = getEquippedItems(Slots.WEAPON);
        if (tempWeapon == null){
            return this.baseDamage*(1+ getPrimaryAttribute()/(double)100);
        }
        Weapon tempWeapon1 = (Weapon)tempWeapon;
        return tempWeapon1.getWeaponDps()*(1+ getPrimaryAttribute()/(double)100);
    }

    //Gets the baseDamage without any weapon.
    public double getBaseDamage() {
        return baseDamage;
    }
}
