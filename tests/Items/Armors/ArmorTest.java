package Items.Armors;

import Utils.ArmorTypes;
import Utils.Slots;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ArmorTest {

    @Test
    public void checkValueOfArmor(){
        //Arrange
        Armor leatherarmor = new Armor("Leather armor",3, Slots.BODY,
                ArmorTypes.LEATHER,new int[]{1,1,1});

        String expectedName = "Leather armor";
        int expectedLevel =3;
        Slots expectedSlot = Slots.BODY;
        ArmorTypes expectedType = ArmorTypes.LEATHER;
        int[] expectedStatBoost =  {1,1,1};

        //Act && Assert
        assertEquals(expectedName,leatherarmor.getName());
        assertEquals(expectedLevel,leatherarmor.getLevelRequirement());
        assertEquals(expectedSlot,leatherarmor.getEquipSlot());
        assertEquals(expectedType,leatherarmor.getType());
        assertEquals(expectedStatBoost[0],leatherarmor.getStatIncrease(0));
        assertEquals(expectedStatBoost[1],leatherarmor.getStatIncrease(1));
        assertEquals(expectedStatBoost[2],leatherarmor.getStatIncrease(2));
    }
}